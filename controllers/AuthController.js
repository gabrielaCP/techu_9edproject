const requestJson = require('request-json');
const crypt = require('../crypt');
const jwtFile = require('../jwt');
const jwt = require('jsonwebtoken');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechu9pro/collections/";
const mlabApiKey = "apiKey=" + process.env.MLAB_API_KEY;

function login(req, res){

    console.log("POST /v1/login");

    if(req.body.email != null && req.body.password != null){

      var mailUser = req.body.email;
      var passUser= req.body.password;

      console.log("mailUser: "+mailUser+ " passUser: "+passUser);

      var queryConsulta = 'q={"email": "'+mailUser+'"}';
      var httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("user/?"+queryConsulta+"&"+mlabApiKey,
       function(err, resMLab, body){
         if (err){
           res.status(401).send({"msg" : "Error obteniendo usuario"});
         }else{
            console.log("Ha ido bien");
           if(body.length > 0){
             var response = body[0];
             var user = body[0];
             passwordFromDBHashed = response.password;
             var customerId = response.customerId;
             var mailUsuario = response.email;
             console.log("passwordFromDBHashed " + passwordFromDBHashed);
             console.log("crypt.hash(passUser) "+crypt.hash(passUser));
             if((crypt.hash(passUser) == passwordFromDBHashed) || (crypt.checkpassword(passUser,passwordFromDBHashed))){
                 console.log("Coinciden");
                 //Hacemos la actualización y enviamos el token
                 var putBody = '{"$set": {"logged": true}}';
                 console.log(putBody);

                 httpClient.put("user?"+queryConsulta+"&"+mlabApiKey, JSON.parse(putBody),
                   function(err, resMLab, body){
                       if (err){
                         console.log(resMLab);
                         res.status(409).send({"msg" : "Error modificando usuario"});
                       }else{
                         console.log("Usuario logado con éxito : id "+customerId);
                         var token = jwtFile.createToken(mailUsuario);
                         res.setHeader('token', token);

                         var response = {"msg" : "Usuario logado con éxito","customerId" : customerId, "user": user};
                          res.status(200).send(response);
                      }
                   }
                 )
             }else{
               res.status(401).send({"msg" : "El usuario o password son incorrectos"});
             }

           }else{
             res.status(409).send({"msg" : "Usuario no encontrado"});
           }
         }
       });
  }else{
    res.status(409).send({"msg": "Parámetros obligatorios no informados"});
  }
}

function logout(req, res){

    console.log("POST  /v1/logout/:customerId");
    console.log("req.headers['token']" + req.headers['token']);
    console.log("customerId" + req.params.customerId);
    if(req.params.customerId != null && req.headers['token']!= null){
      var customerId = req.params.customerId;
      var httpClient = requestJson.createClient(baseMlabURL);
      console.log("customerId: "+customerId);
      var query = 'q={"customerId": '+customerId+'}';

      httpClient.get("user/?"+query+"&"+mlabApiKey,
       function(err, resMLab, body){
         if (err){
           var response = {"msg" : "Error obteniendo usuario"}
           res.status(409).send(response);
         }else{
            console.log("Ha ido bien");
            var response = body[0];
            var logged = response.logged;
            var idUsuario= response.customerId;
            console.log("logado: " + logged);
            if(jwtFile.verifyToken(req.headers['token'])){
              var putBody = '{"$unset": {"logged": ""}}';
              console.log(putBody);
              //Hacemos la query con el ID que recogemos de la consulta por si acaso
              var queryPUT = 'q={"customerId": '+idUsuario+'}';
              httpClient.put("user?"+queryPUT+"&"+mlabApiKey, JSON.parse(putBody),
                  function(errPUT, resMLabPUT, bodyPUT){
                      if (errPUT){
                          var response = {"msg" : "Error modificando usuario"}
                          console.log(errPUT);
                          res.status(409).send(response);
                      }else{
                          console.log("Usuario deslogado con éxito : customerId "+customerId);
                          console.log(bodyPUT);
                         res.status(200);
                         var response = {"msg" : "usuario deslogado", "customerId" : customerId};
                         res.send(response);
                     }
                  }
                ) //Cierre put
            }else{
              res.status(401).send({"msg" : "Error verificando token"});
            }
          }//cierre else
        });// cierre get
  }else{
    res.status(400).send({"msg" : "Parámetros obligatorios no informados"});
  }
}

module.exports.login= login;
module.exports.logout = logout;
