const requestJson = require('request-json');
const crypt = require('../crypt');
const dateTime = require('node-datetime');
const jwtFile = require('../jwt');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechu9pro/collections/";

const mlabApiKey = "apiKey=" + process.env.MLAB_API_KEY;
console.log(mlabApiKey); //"apiKey=RNNUfiWJrCqaCCoV8rB9ArGP1JEQv4Mv"; //Ojo, para la petición http se tiene que devolver el apiKey con el valor

function getUser(req,res) {
   console.log("GET /v1/users/:customerId");
   var token = req.headers['token'];

   //Validación del token:
   var resultToken= jwtFile.verifyToken(token);
   console.log(JSON.stringify(resultToken));

   if(resultToken.status !=200){
     res.send(resultToken);
   }else if(req.params.customerId == null){
     res.status(400).send({"msg" : "Error datos obligatorios no informados"});
   }else{
     //Token correcto
     var httpClient = requestJson.createClient(baseMlabURL);
     var customerId = req.params.customerId;
     console.log("customerId: "+customerId);
     var query = 'q={"customerId": '+customerId+'}';

     httpClient.get("user/?"+query+"&"+mlabApiKey,
         function(err, resMLab, body){
           if (err){
               res.status(500).send({"msg" : "Error obteniendo usuario"});
           }else{
              if(body.length > 0){
               var response = {
                 "customerId" : body[0].customerId,
                 "firstName": body[0].firstName,
                 "lastName": body[0].lastName,
                 "birthDate": body[0].birthDate,
                 "email": body[0].email,
                 "age": body[0].age,
                 "NIF": body[0].NIF,
                 "identityDocument":{
                   "documentType": {
                     "id": body[0].identityDocument.documentType.id,
                     "name": body[0].identityDocument.documentType.name
                  },
                 "documentNumber": body[0].identityDocument.documentNumber
               },
               "registrationDate": body[0].registrationDate
               };
               res.status(200).send(response);
             }else{
               res.status(404).send({"msg" : "Usuario no encontrado"});
             }
           }
         });
      }
}

function listUsers(req,res) {
   console.log("GET /v1/users/");
   var httpClient = requestJson.createClient(baseMlabURL);
   httpClient.get("user/?"+mlabApiKey,
       function(err, resMLab, body){
         if (err){
             console.log(resMLab);
             res.status(500).send({"msg" : "Error obteniendo usuario"})
         }else{

            var response =(body.length == 0)? {"msg": "No existen datos"}:body;
            var statusValue= (err)? 204: 200;
            res.status(statusValue).send(response);
         }
       });
}

function createUser(req, res){
  console.log('POST /v1/users/');
  //Validamos los datos obligatorios
  var newUser= validarAltaCliente(req, res);
  console.log(JSON.stringify(newUser));

  if(newUser.err == null){
      //Validamos que el cliente no esté ya registrado:
      var query = 'q={"email":"'+req.body.email+'"}';
      var httpClient = requestJson.createClient(baseMlabURL);

      httpClient.get("user/?"+query+"&"+mlabApiKey,function(err, resMLab, body){
        if(body.length != 0 && (body[0].email != null && body[0].email == req.body.email)){
            res.status(409).send({"err": "Error", "msg" : "El usuario ya está registrado"});
        }else{
            //Consulto el último usuario para obtener el id
            var query = 'f={"customerId" : 1}&s={"customerId" : -1}&l=1';
            //Hago la petición para obtener el último registro y conocer su id
            httpClient.get("user/?"+query+"&"+mlabApiKey,
              function(err, resMLab, body){
               if (err){
                 console.log(resMLab);
                 res.status(409).send({"msg" : "Error obteniendo usuario"});
               }else{
                 var idNueva = (body.length > 0 && body[0].customerId != null)? ((body[0].customerId)+1) : 1;
                 console.log("Id del nuevo usuario: "+ idNueva);
                 //Concatenamos para asignar el customerId y la fecha de alta
                 newUser= Object.assign(newUser,
                   {"customerId" : idNueva,
                   "registrationDate": dateTime.create(new Date()).now()
                    }
                  );
                httpClient.post("user/?"+mlabApiKey, newUser,
                  function(err, resMLab, body){
                     var response= (err)? {"msg": "Error insertando cliente"}:{"customerId": idNueva, "registrationDate": dateTime.create(new Date()).now()};
                     var statusValue=  (err)? 500: 201;
                     res.status(statusValue).send(response);
                });
            }
          });
      }
    });
 }
}

function validarAltaCliente(req, res){
  //Validamos los datos obligatorios:
  console.log("Validamos los datos obligatorios");
  console.log(JSON.stringify(req.body));
  if(req.body.firstName == null || req.body.lastName == null || req.body.email == null || req.body.password == null ||
    req.body.identityDocument.documentType.id == null || req.body.identityDocument.documentNumber == null ||
    req.body.firstName == "" || req.body.lastName == "" || req.body.email == "" || req.body.password == "" ||
      req.body.identityDocument.documentType.id == "" || req.body.identityDocument.documentNumber == ""){

      var response = {  "err": "Error", "msg" : "Parámetros obligatorios no informados"};
      res.status(400).send(response);
      return response;
  }else{
    // Construimos la estructura del documento a insertar en BBDD:
    var newUser = {
      "firstName": req.body.firstName,
      "lastName": req.body.lastName,
      "birthDate": req.body.birthDate,
      "email": req.body.email,
      "age": req.body.age,
      "NIF": req.body.NIF,
      "identityDocument":{
        "documentType":{
          "id": req.body.identityDocument.documentType.id,
          "name": req.body.identityDocument.documentType.name
        },
        "documentNumber": req.body.identityDocument.documentNumber
      },
      "password" : crypt.hash(req.body.password)};

    return newUser;
  }
}

function modifyUser(req,res){
  console.log("PUT /users/v1/:customerId");

   var token = req.headers['token'];
   //Validación del token:
   var resultToken= jwtFile.verifyToken(token);

   if(resultToken.status !=200){
     res.send(resultToken);
   }else if(req.params.customerId == null){
     res.status(400).send({"msg" : "Error datos obligatorios no informados"});
   }else{
     //Token correcto
     var putBodyJSON = validardatosPutCliente(req, res);
     var customerId=req.params.customerId;
     var httpClient = requestJson.createClient(baseMlabURL);
     var query = '?q={"customerId":'+customerId+'}';
     httpClient.get("user/?"+query+"&"+mlabApiKey, function (errGet, resGet, bodyGet){
        if (errGet) {
          res.status(404).send({"msg":"Error obteniendo usuario"});
        }else{
          if(bodyGet !=null && bodyGet.length>0){
              var query = '?q={"id":'+customerId+'}&u=true&';
              var putBody='{"$set":'+JSON.stringify(putBodyJSON)+'}';
              console.log("putBody: "+putBody);
              httpClient.put("user/?"+query+mlabApiKey, JSON.parse(putBody), function (errPut, resMLabPut, bodyPut){
                if (!errPut){
                    res.status(200).send({"updateDate": putBodyJSON.updateDate, "customerId":req.params.customerId });
                }else{
                    res.status(409).send({"msg":"Error modificando el usuario"});
                }
              });
            }else{
              res.status(409).send({"msg":"Error obteniendo el usuario a modificar"});
            }
        }
    });
 }
}

function validardatosPutCliente(req, res){
  //Validamos si se han informado parámetros modificables:

  if(req.body.firstName == null && req.body.lastName == null &&  req.body.password && req.body.birthDate
     && req.body.identityDocument== null){
    var response = {"err": "Error", "msg" : "Es necesario indicar los datos a modificar"};
    res.status(400).send(response);
    return response;
  }else if(req.body.email != null){
    var response = {"err": "Error", "msg": "Modificación no permitida"};
    res.status(409).send(response);
    return response;
  }else{
    //Componemos el JSON que se insertará en BBDD:

    var idenJson ={};
    var idenDocJson= {};
    var putJson= {"updateDate": dateTime.create(new Date()).now()};
    if(req.body.firstName != null) putJson= Object.assign(putJson,{"firstName": req.body.firstName});
    if(req.body.lastName != null) putJson = Object.assign(putJson, {"lastName": req.body.lastName});
    if(req.body.birthDate != null) putJson = Object.assign(putJson, {"birthDate": req.body.birthDate});
    if(req.body.password != null) putJson = Object.assign(putJson, {"password": crypt.hash(req.body.password)});
    if(req.body.identityDocument != null){
      if(req.body.identityDocument.documentType != null && req.body.identityDocument.documentType.id != null && req.body.identityDocument.documentNumber ){
        console.log(JSON.stringify(req.body.identityDocument.documentType));
        idenJson = Object.assign(idenJson,{"identityDocument":{"documentType":req.body.identityDocument.documentType,"documentNumber": req.body.identityDocument.documentNumber}});
        console.log(JSON.stringify(idenDocJson));
      }
    //  if(req.body.identityDocument.documentNumber != null) idenDocJson = Object.assign(idenDocJson,{"identityDocument":{"documentNumber": req.body.identityDocument.documentNumber}});
    }

    //idenJson= Object.assign(idenJson,idenDocJson);
    putJson = Object.assign(putJson,idenJson);
    console.log(JSON.stringify(putJson));
    return putJson;
  }
}

module.exports.getUser = getUser;
module.exports.listUsers = listUsers;
module.exports.createUser = createUser;
module.exports.modifyUser = modifyUser;
