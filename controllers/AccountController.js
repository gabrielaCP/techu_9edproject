const requestJson = require('request-json');
const crypt =require('../crypt');
const jwtFile = require('../jwt');
const dateTime = require('node-datetime');
const jwt = require('jsonwebtoken');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechu9pro/collections/";
const mlabApiKey = "apiKey=" + process.env.MLAB_API_KEY;

/**
*Crear cuenta: en primer lugar valida el token del usuario logado: si es correcto, obtiene las cuentas del usuario:
si aún no tiene, inicia el identificador de la cuenta. Si tiene calcula el id correspondiente de la cuenta.
Si se asigna balance, se genera un movimiento. Si no, no se genera.
Se devuelve el id de la cuenta.
*/
function createAccount(req, res){
  console.log('/v1/users/:customerId/accounts');

  var token = req.headers['token'];

  //Validación del token:
  var resultToken= jwtFile.verifyToken(token);
  console.log(JSON.stringify(resultToken));
  var estado = resultToken.status;
  console.log(estado);
  if(estado !=200){

    res.send(resultToken);

  }else if(req.params.customerId == null ){
    res.status(400).send({"err": "Parámetros obligatorios no informados"});
  }else{

    //eL TOKEN es correcto. Invocamos al método para crear la cuenta.
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");
    //El id de la cuenta debe ser universal e independiente del cliente

    console.log("Comenzamos las consultas: primero hacemos un get en accounts");
    httpClient.get("account/?"+mlabApiKey,
       function(err, resMLab, body){
         if (err){
           var response = {
             "msg" : "Se ha producido un error realizando la consulta de cuentas"
           }
           console.log("Error en consulta de cuentas");
           res.status(500).send(response);
         }else{
           console.log("Tras consulta de cuentas: analizamos si existen cuentas de cualquier usuario");
           //consultamos si hay cuentas anteriores
           var idNueva = 1;
           if(body.length > 0){

             idNueva = 1+ Number(body.length);

            console.log("Id de la nueva cuenta: "+ idNueva);
         }
         //Generamos el registro para la nueva cuenta:
         //generamos IBAN
         var iban = generateIban();
         console.log("Se ha generado el iban -->" + iban);
         //Revisamos si se indica balance o no:
         var balanceIn= (req.body.balance != null && req.body.balance.amount != null && req.body.balance.amount > 0)? req.body.balance.amount : 0;
         var divisa = (req.body.balance != null && req.body.balance.currency != null)? req.body.balance.currency : "EUR";

         var creaMovimiento = false;

         if(balanceIn != null && balanceIn != 0 && balanceIn >0){
           amount= balanceIn;
           creaMovimiento= true;
           console.log("Vamos a crear el movimiento además de la cuenta: "+balanceIn);
         }

         //Generamos el JSON de la cuenta con el formato correcto para insertarlo en BBDD
         var newAccount = {
                    "customerId" : Number(req.params.customerId),
                    "accountId": idNueva,
                    "alias": (req.body.alias != null)? (req.body.alias).toUpperCase() : "Cuenta Corriente",
                    "IBAN": iban,
                    "creationDate": new Date(),//dateTime.create(new Date()).now().toLocaleString(),
                    "blocked": false,
                    "productType": {
                      "id" : (req.body.productType != null && req.body.productType.id != null)? req.body.productType.id: "0020",
                      "name" : (req.body.productType != null && req.body.productType.name != null)? req.body.productType.name: "CUENTA",
                    },
                    "balance": {
                        "amount": balanceIn,
                        "currency": divisa
                    }
                   };

         httpClient.post("account/?"+mlabApiKey, newAccount,
               function(errC, resMLabC, bodyC){
                 if(err){
                   console.log(resMLabC);
                   res.status(409).send({"msg" : "Error creando cuenta"});
                 }else{
                   console.log("Cuenta creada con éxito");
                   console.log(bodyC);

                   if(creaMovimiento){
                     //Genero el JSON con la estructura correcta para guardarlo en BBDD
                     var transactionId=getRandomInt(dateTime.create(new Date()).now(),(dateTime.create(new Date()).now())+100000000);
                     var newTransaction= {
                       "transactionId": transactionId,
                       "accountId": idNueva,
                       "originAmount":{
                         "amount":amount,
                         "currency": divisa
                       },
                       "operationDate":  dateTime.create(new Date()).now(),
                       "operationType": "ABONO",
                       "concept": "Apertura cuenta"
                     }

                     httpClient.post("transaction/?"+mlabApiKey, newTransaction,
                           function(errTransaction, resMLabT, bodyTransaction){
                             if(err){
                                 var response = {
                                   "msg" : "Se ha producido un error creando movimiento"
                                 }
                                 console.log(resMLabT);
                                 res.status(500).send(response);
                             }else{
                                  console.log("Movimiento creado correctamente");
                                  res.status(201).send({
                                      "msg" : "cuenta creada",
                                      "accountId": idNueva,
                                      "transactionId": transactionId});
                             }
                   })

                 }else{
                   res.status(201).send({"msg" : "Cuenta creada", "accountId": idNueva});
                 }

               }
            })
          }
        })
      }
}



function listAccount(req, res){
  console.log('/v1/users/:customerId/accounts/');

  var token = req.headers['token'];

  //Validación del token:
  var resultToken= jwtFile.verifyToken(token);
  var estado = resultToken.status;
  console.log(estado);
  if(estado !=200){
    res.send(resultToken);
  }else{
    if(req.params.customerId != null){
      var customerId = Number(req.params.customerId);
      console.log("customerId: "+customerId);

  var queryAccount ='q={"customerId": '+customerId+'}';
  console.log(baseMlabURL+"account/?"+queryAccount+"&"+mlabApiKey);

  //Consultamos la cuenta
  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.get("account/?"+queryAccount+"&"+mlabApiKey,
       function(err, resMLab, body){
         if(err){
           var response = {
               "msg" : "Error obteniendo las cuentas del usuario"
           }
           res.status(409).send(response);
         }else{
           console.log(body);
           res.status(200).send(body);
         }
      });
    }else{
      var response = {
          "msg" : "Error parámetros entrada"
      }
      res.status(400).send(response);
    }
  }
}

/**
*Consulta el detalle de una cuenta de un cliente
*/
function getAccount(req, res){
  console.log('/v1/users/:id/accounts/:id1');
  var token = req.headers['token'];

  //Validación del token:
  var resultToken= jwtFile.verifyToken(token);

  if(resultToken.status !=200){
    res.send(resultToken);

  }else{
    //Validación Token Correcta

    if(req.params.customerId != null && req.params.accountId != null){

      var customerId = Number(req.params.customerId);
      var accountId = Number(req.params.accountId);
      console.log("customerId: "+customerId+" accountId: " +accountId);

      var queryAccount ='q={"$and": [{"customerId":'+customerId+'},{"accountId": '+accountId+'}]}';
      console.log(baseMlabURL+"account/?"+queryAccount+"&"+mlabApiKey);

      //Consultamos la cuenta
      var httpClient = requestJson.createClient(baseMlabURL);
      httpClient.get("account/?"+queryAccount+"&"+mlabApiKey,
       function(err, resMLab, body){
         if(err){
           var response = {
               "msg" : "Error obteniendo la cuenta del usuario"
           }
           res.status(500).send(response);
         }else{
           if(body.length >0){
             console.log(body);
             res.status(200).send(body[0]);
           }else{
             var response = {
                 "msg" : "No existe la cuenta para este cliente"
             }
             res.status(409).send(response);
           }

         }
      });
    }else{
      var response = {
          "msg" : "Error parámetros entrada"
      }
      res.status(400).send(response);
    }
    }
}

function deleteAccount(req, res){
  console.log('/v1/users/:id/accounts/id2');
  var token = req.headers['token'];

  //Validación del token:
  var resultToken= jwtFile.verifyToken(token);

  if(resultToken.status !=200){
    res.send(resultToken);

  }else{
    //Validación Token Correcta

    if(req.params.customerId != null && req.params.accountId != null){

      var customerId = Number(req.params.customerId);
      var accountId = Number(req.params.accountId);
      console.log("customerId: "+customerId+" accountId: " +accountId);

      var queryAccount ='q={"$and": [{"customerId":'+customerId+'},{"accountId": '+accountId+'}]}';
      console.log(baseMlabURL+"account/?"+queryAccount+"&"+mlabApiKey);
        var putBody ="[]";
      //Consultamos la cuenta
      var httpClient = requestJson.createClient(baseMlabURL);
      httpClient.put("account/?"+queryAccount+"&"+mlabApiKey,JSON.parse(putBody),
       function(err, resMLab, body){
         if(err){
           var response = {
               "msg" : "Error obteniendo la cuenta del usuario"
           }
           res.status(500).send(response);
         }else{
           if(body.removed>0 ){
             console.log(body);
             httpClient.put("transaction/?"+queryAccount+"&"+mlabApiKey, JSON.parse(putBody),
                   function(errTransaction, resMLabT, bodyTransaction){
                     if(err){
                         var response = {"msg" : "Se ha producido un error eliminando los movimientos"}
                         console.log(resMLabT);
                         res.status(500).send(response);
                     }else{
                          console.log("Movimientos borrados correctamente");
                          res.status(200).send({"msg" : "Cuenta y Movimientos borrados con éxito"});
                     }
                   });
            }
          }
        });
    }else{
      var response = {
          "msg" : "Error parámetros entrada"
      }
      res.status(400).send(response);
    }
}
}
/**
* Genera un número aleatorio
* @param {double} min valor mínimo
* @param {double} max valor máximo
* @returns {double} número aleatorio
*/
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

/**
* Genera un número aleatorio
* @param {integer} longitud longitud del valor aleatorio
* @returns {String} valor alfanumérico generado
*/
function getRandom(longitud){
  var caracteres = "0123456789";
  var salida = "";
  for (i=0; i<longitud; i++) salida += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
  return salida;
}

function generateIban(){

  var digCont = getRandomInt(01,99);
  var sucursal = getRandom(4);
  var bban = getRandom(4);
  var cuenta = getRandom(4);
  var iban = "ES" + digCont + " 0182 " + sucursal + " "+ bban + " " + cuenta;
  console.log("Código iban -->" + iban);
  return iban;
}

module.exports.createAccount = createAccount;
module.exports.getAccount = getAccount;
module.exports.listAccount = listAccount;
module.exports.deleteAccount = deleteAccount;
